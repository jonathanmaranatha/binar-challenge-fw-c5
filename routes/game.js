const express = require('express');
const router = express.Router();

/* GET play page. */
router.get('/', function (req, res, next) {
    if (req.session.userId) {
        res.render('play', { choices: ["batu", "kertas", "gunting"] });
    } else {
        res.redirect('/login');
    }
});

module.exports = router;