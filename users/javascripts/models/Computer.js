import { Player } from "./Player";

export class Computer extends Player {

    choose() {
        super.choose(Math.floor(Math.random() * 3));
    }
}