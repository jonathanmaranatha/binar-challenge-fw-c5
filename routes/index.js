const express = require('express');
const router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  loginNav = "";

  if (req.session.userId) {
    loginNav = `<li class="nav-item">
                <a class="nav-link" href="/logout">LOGOUT</a>
              </li>`;
  } else {
    loginNav = `<li class="nav-item">
                <a class="nav-link" href="#">SIGN UP</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/login">LOGIN</a>
              </li>`;
  }

  res.render('index', { login: loginNav });
});

module.exports = router;